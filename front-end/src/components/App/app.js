import React from 'react'
import axios from 'axios'
import './app.css'

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { name: '' }
  }

  componentDidMount() {
    axios.get(process.env.REACT_APP_API_URL + 'v1/summoners/name/Arka').then(resp => {
      this.setState({name: resp.data.name})
    })
  }

  render() {
    return (
      <div className="App">
        <div>{this.state.name}</div>
      </div>
    );
  }
}

export default App;
