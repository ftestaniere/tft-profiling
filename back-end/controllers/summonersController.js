var axios = require('axios');

exports.getSummoner = function(req, res) {
    axios.get('https://euw1.api.riotgames.com/tft/summoner/v1/summoners/by-puuid/'+ req.params.puuid +'?api_key='+ process.env.API_KEY)
        .then(response => {
            res.send(response.data)
        })
}

exports.getSummonerByName = function(req, res) {
    axios.get('https://euw1.api.riotgames.com/tft/summoner/v1/summoners/by-name/'+ req.params.name +'?api_key='+ process.env.API_KEY)
        .then(response => {
            res.send(response.data)
        })
}