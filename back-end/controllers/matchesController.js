var axios = require('axios');

exports.getMatchesId = function(req, res) {
    axios.get('https://euw1.api.riotgames.com//tft/match/v1/matches/by-puuid/'+ req.params.puuid +'/ids?api_key='+ process.env.API_KEY)
        .then(response => {
            res.send(response.data)
        })
}

exports.getMatchInfo = function(req, res) {
    axios.get('https://euw1.api.riotgames.com//tft/match/v1/matches/'+ req.params.matchId +'?api_key='+ process.env.API_KEY)
        .then(response => {
            res.send(response.data)
        })
}