var express = require('express');
var router = express.Router();

var summoner_controller = require('../controllers/summonersController')

// middleware logger
router.use(function timeLog (req, res, next) {
    console.log('[' + new Date(Date.now()).toLocaleString() + '][SUMMONER]' + req.baseUrl + req.path + ' -> ' + req.ip)
    next()
})

router.get('/name/:name', summoner_controller.getSummonerByName)
router.get('/puuid/:puuid', summoner_controller.getSummoner)

module.exports = router;