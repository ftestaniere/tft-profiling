var express = require('express');
var router = express.Router();

var matches_controller = require('../controllers/matchesController')

// middleware logger
router.use(function timeLog (req, res, next) {
    console.log('[' + new Date(Date.now()).toLocaleString() + '][MATCH]' + req.baseUrl + req.path + ' -> ' + req.ip)
    next()
})

router.get('/puuid/:puuid', matches_controller.getMatchesId)
router.get('/id/:matchId', matches_controller.getMatchInfo)

module.exports = router;