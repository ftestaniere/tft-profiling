const dotenv = require('dotenv')
const express = require('express')
var cors = require('cors')
const app = express();

dotenv.config()

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}
app.use(cors(corsOptions))

var summonerRouter = require('./routes/summoners')
var matchRouter = require('./routes/matches')

app.use('/v1/summoners', summonerRouter)
app.use('/v1/matches', matchRouter)

app.listen(process.env.PORT, () => {
  console.log(`Server listening on port ${process.env.PORT}!`)
});
